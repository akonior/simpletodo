package pl.itako.simpletodo.models.filter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class QueryParserTest {

    private QueryParser parser;

    @Before
    public void before() {
        parser = new QueryParser();
    }

    @Test
    public void testSectionTitleAndPredicate() {
        check("p1", "Priority 1", "PriorityPredicate");
        check("   p1  ", "Priority 1", "PriorityPredicate");
        check("   p1", "Priority 1", "PriorityPredicate");
        check("2015-12-12", "Dec 12", "DatePredicate");
        check("    2000-12-12  ", "Dec 12", "DatePredicate");
        check("qwerty", "Search for: 'qwerty'", "ContentPredicate");
        check("qwerty abc", "Search for: 'qwerty abc'", "ContentPredicate");
        check("(p1|abc&(2000-12-12&p4))",
                "(Priority 1 | Search for: 'abc' & (Dec 12 & Priority 4))",
                "((PriorityPredicate OR ContentPredicate) AND (DatePredicate AND PriorityPredicate))");
        check("err & p1",
                "Search for: 'err ' & Priority 1",
                "(ContentPredicate AND PriorityPredicate)");
        check("err&p1",
                "Search for: 'err' & Priority 1",
                "(ContentPredicate AND PriorityPredicate)");
        check("(err)", "(Search for: 'err')", "ContentPredicate");
        check(" p1", "Priority 1", "PriorityPredicate");
        check(" (err)", "(Search for: 'err')", "ContentPredicate");
        check(" (p1)", "(Priority 1)", "PriorityPredicate");
        check("error & ( 2015-08-02 | 2015-08-04 ) & ( p1 | p2 )",
                "Search for: 'error ' & (Aug 02 | Aug 04) & (Priority 1 | Priority 2)",
                "((ContentPredicate AND (DatePredicate OR DatePredicate)) AND (PriorityPredicate OR PriorityPredicate))");
        check("error&(2015-08-02|2015-08-04)&(p1|p2)",
                "Search for: 'error' & (Aug 02 | Aug 04) & (Priority 1 | Priority 2)",
                "((ContentPredicate AND (DatePredicate OR DatePredicate)) AND (PriorityPredicate OR PriorityPredicate))");
        check("p4   | error  abc  & (2015-08-02 | p3 & xyz qwertyuiop      )  &(p1|2015-08-02)",
                "Priority 4 | Search for: 'error  abc  ' & (Aug 02 | Priority 3 & Search for: 'xyz qwertyuiop      ') & (Priority 1 | Aug 02)",
                "(((PriorityPredicate OR ContentPredicate) AND ((DatePredicate OR PriorityPredicate) AND ContentPredicate)) AND (PriorityPredicate OR DatePredicate))");
    }

    private void check(String input, String expectedTitle, String expectedPredicate) {
        PreparedQuery value = parser.parse(input);
        assertEquals(expectedTitle, value.title);
        assertEquals(expectedPredicate, value.predicate.toString());
    }
}