grammar TodoQuery;

@header {
    package pl.itako.simpletodo.models.filter;
}

NUMBER   : [0-9];
WORDS    : [a-zA-Z][a-zA-Z ]*;
WS     : [ \t\r]+ -> skip;
AND   : '&';
OR    : '|';
LPAR  : '(';
RPAR  : ')';

tquery    : 'p' | WORDS;
tdate     : NUMBER NUMBER NUMBER NUMBER '-' NUMBER NUMBER '-' NUMBER NUMBER;
tpriority : 'p' NUMBER;

andExpr
    : andExpr AND orExpr # And
    | orExpr             # ToOrExpr
    ;

orExpr
    : orExpr OR term    # Or
    | term              # ToTerm
    ;

term
    : tquery             # Query
    | tdate              # Date
    | tpriority          # Priority
    | LPAR andExpr RPAR # Braces
    ;