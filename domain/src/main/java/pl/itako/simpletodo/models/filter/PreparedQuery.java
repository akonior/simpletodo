package pl.itako.simpletodo.models.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.itako.simpletodo.models.Item;

public class PreparedQuery {

    public final String title;
    public final Predicate<Item> predicate;

    public PreparedQuery(String title, Predicate<Item> predicate) {
        this.title = title;
        this.predicate = predicate;
    }

    public static abstract class AbstractPredicate implements Predicate<Item> {
        @Override public String toString() {
            return getClass().getSimpleName();
        }
    }

    public static class DatePredicate extends AbstractPredicate {

        public final Date dueDate;
        private static SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

        public DatePredicate(Date dueDate) {
            this.dueDate = dueDate;
        }

        @Override
        public boolean apply(Item input) {
            return fmt.format(dueDate).equals(fmt.format(input.dueDate));
        }
    }

    public static class ContentPredicate extends AbstractPredicate {

        private final String query;

        public ContentPredicate(String query) {
            this.query = query;
        }

        @Override
        public boolean apply(Item input) {
            return input.content.toLowerCase().contains(query.trim().toLowerCase());
        }
    }

    public static class PriorityPredicate extends AbstractPredicate {

        public final int priority;

        public PriorityPredicate(int priority) {
            this.priority = priority;
        }

        @Override
        public boolean apply(Item input) {
            return input.priority == priority;
        }
    }

    public static class AndPredicate extends AbstractPredicate {

        public final Predicate<Item> left;
        public final Predicate<Item> right;

        public AndPredicate(Predicate<Item> left, Predicate<Item> right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean apply(Item input) {
            return left.apply(input) && right.apply(input);
        }

        @Override public String toString() {
            return "(" + left + " AND " + right + ")";
        }
    }

    public static class OrPredicate extends AbstractPredicate {

        private final Predicate<Item> left;
        private final Predicate<Item> right;

        public OrPredicate(Predicate<Item> left, Predicate<Item> right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean apply(Item input) {
            return left.apply(input) || right.apply(input);
        }

        @Override public String toString() {
            return "(" + left + " OR " + right + ")";
        }
    }

    public interface Predicate<T> {
        boolean apply(T t);
    }

    public static class TruePredicate extends AbstractPredicate {

        @Override public boolean apply(Item o) {
            return true;
        }
    }
}
