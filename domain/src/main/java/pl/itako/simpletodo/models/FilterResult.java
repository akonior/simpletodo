package pl.itako.simpletodo.models;

import java.util.List;

public class FilterResult {
    public final Section section;
    public final List<Item> items;

    public FilterResult(Section section, List<Item> items) {
        this.section = section;
        this.items = items;
    }
}