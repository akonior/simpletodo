package pl.itako.simpletodo.models.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TodoQueryTreeVisitor extends TodoQueryBaseVisitor<PreparedQuery> {

    SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    SimpleDateFormat outputDateFormat = new SimpleDateFormat("MMM dd", Locale.getDefault());

    @Override public PreparedQuery visitAnd(TodoQueryParser.AndContext ctx) {
        PreparedQuery left = visit(ctx.andExpr());
        PreparedQuery right = visit(ctx.orExpr());
        return new PreparedQuery(left.title + " & " + right.title, new PreparedQuery.AndPredicate(left.predicate, right.predicate));
    }

    @Override public PreparedQuery visitOr(TodoQueryParser.OrContext ctx) {
        PreparedQuery left = visit(ctx.orExpr());
        PreparedQuery right = visit(ctx.term());
        return new PreparedQuery(left.title + " | " + right.title, new PreparedQuery.OrPredicate(left.predicate, right.predicate));
    }

    @Override public PreparedQuery visitPriority(TodoQueryParser.PriorityContext ctx) {
        String pN = ctx.getText();
        String text = pN.substring(1);
        return new PreparedQuery("Priority " + text, new PreparedQuery.PriorityPredicate(Integer.decode(text)));
    }

    @Override public PreparedQuery visitDate(TodoQueryParser.DateContext ctx) {
        PreparedQuery.Predicate predicate = new PreparedQuery.TruePredicate();
        String title = "";
        if (ctx.getText().length() == 10) {
            try {
                Date dueDate = inputDateFormat.parse(ctx.getText());
                predicate = new PreparedQuery.DatePredicate(dueDate);
                title = outputDateFormat.format(dueDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return new PreparedQuery(title, predicate);
    }

    @Override public PreparedQuery visitQuery(TodoQueryParser.QueryContext ctx) {
        return new PreparedQuery("Search for: '" + ctx.getText() + "'", new PreparedQuery.ContentPredicate(ctx.getText()));
    }

    @Override public PreparedQuery visitBraces(TodoQueryParser.BracesContext ctx) {
        PreparedQuery inside = visit(ctx.andExpr());
        return new PreparedQuery("(" + inside.title + ")", inside.predicate);
    }
}
