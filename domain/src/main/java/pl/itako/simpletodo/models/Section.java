package pl.itako.simpletodo.models;

import java.util.Date;

import pl.itako.simpletodo.models.filter.PreparedQuery;

public class Section {
    public final String title;
    public final Date dueDate;
    public final Integer priority;

    public Section(String title, Date dueDate, Integer priority) {
        this.title = title;
        this.dueDate = dueDate;
        this.priority = priority;
    }

    public static Section from(PreparedQuery preparedQuery) {
        if (preparedQuery == null) {
            return new Section("Incorrect query", null, null);
        } else {
            String title = preparedQuery.title;
            Date dueDate = determineDueDate(preparedQuery.predicate);
            Integer priority = determinePriority(preparedQuery.predicate);
            return new Section(title, dueDate, priority);
        }
    }

    private static Date determineDueDate(PreparedQuery.Predicate predicate) {
        if (predicate instanceof PreparedQuery.DatePredicate) {
            return ((PreparedQuery.DatePredicate) predicate).dueDate;
        } else if (predicate instanceof PreparedQuery.AndPredicate) {
            Date leftDate = determineDueDate(((PreparedQuery.AndPredicate) predicate).left);
            Date rightDate = determineDueDate(((PreparedQuery.AndPredicate) predicate).right);
            if (leftDate != null && rightDate != null && leftDate.equals(rightDate)) {
                return leftDate;
            }
        }
        return null;
    }

    private static Integer determinePriority(PreparedQuery.Predicate predicate) {
        if (predicate instanceof PreparedQuery.PriorityPredicate) {
            return ((PreparedQuery.PriorityPredicate) predicate).priority;
        } else if (predicate instanceof PreparedQuery.AndPredicate) {
            Integer leftPrio = determinePriority(((PreparedQuery.AndPredicate) predicate).left);
            Integer rightPrio = determinePriority(((PreparedQuery.AndPredicate) predicate).right);
            if (leftPrio != null && rightPrio != null && leftPrio.equals(rightPrio)) {
                return leftPrio;
            }
        }
        return null;
    }

    @Override public String toString() {
        return "Section{" +
                "title='" + title + '\'' +
                ", dueDate=" + dueDate +
                ", priority=" + priority +
                '}';
    }
}
