package pl.itako.simpletodo.models;

import java.util.Date;

public class Item {

    public final String content;
    public final Date dueDate;
    public final int priority;

    public Item(String content, Date dueDate, int priority) {
        this.content = content;
        this.dueDate = dueDate;
        this.priority = priority;
    }
}