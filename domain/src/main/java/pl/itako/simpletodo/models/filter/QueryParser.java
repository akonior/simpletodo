package pl.itako.simpletodo.models.filter;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class QueryParser {

    public PreparedQuery parse(String query) {
        TodoQueryLexer lexer = new TodoQueryLexer(new ANTLRInputStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TodoQueryParser parser = new TodoQueryParser(tokens);
        ParseTree tree = parser.andExpr();
        TodoQueryTreeVisitor todoQueryTreeVisitor = new TodoQueryTreeVisitor();
        try {
            PreparedQuery preparedQuery = todoQueryTreeVisitor.visit(tree);
            return preparedQuery;
        } catch (Exception e) {
            return null;
        }
    }
}
