package pl.itako.simpletodo.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.itako.simpletodo.filter.FilteringEngine;
import pl.itako.simpletodo.R;
import pl.itako.simpletodo.models.FilterResult;
import pl.itako.simpletodo.models.TestData;
import pl.itako.simpletodo.views.adapters.TasksAdapter;

public class MainActivityFragment extends Fragment implements SearchView.OnQueryTextListener {

    @Bind(R.id.query) SearchView queryView;
    @Bind(R.id.querySummary) TextView querySummary;
    @Bind(R.id.recyclerView) RecyclerView recyclerView;

    FilteringEngine filteringEngine = new FilteringEngine();

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        queryView.setOnQueryTextListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new TasksAdapter(getActivity(), TestData.TEST_ITEMS));

        return view;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        FilterResult filter = filteringEngine.filter(query);
        querySummary.setText(filter.section.title);
        recyclerView.swapAdapter(new TasksAdapter(getActivity(), filter.items), false);
        return false;
    }
}
