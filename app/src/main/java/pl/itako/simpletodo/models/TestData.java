package pl.itako.simpletodo.models;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TestData {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static final List<Item> TEST_ITEMS = new ArrayList<>();

    static {
        TEST_ITEMS.add(new Item("start failed", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Support automatic build flavors per density to reduce apk size for low storage phones.", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Please remove the write protection on External SD card on Adroid 4.4.2 I would like the ability to add and remove files from my external SD card. Aug. 15, 2015", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android studio  cant run", new DateTime(2015, 8, 1, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("CoordinatorLayout shows padding under status bar", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Download NDK timeout", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Need for an undo button for typing and text", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Adding immersive mode to everywhere", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Action Bar - adding buttons - seems to need to be updated", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android studio restarter issue after recent update", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android5.1 when battery saver is on and click launcher3 folder won't show app icon and folder name", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android studio 1.4 seems to enabling graphics profiling that leads to growing memory heap on device", new DateTime(2015, 8, 1, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("com.google.android.gms:play-services-ads:7.8.0 AudioManager NullPointerException", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Google Voice Typing doesn't use the Bluetooth headset microphone", new DateTime(2015, 8, 1, 12, 0).toDate(), 3));
        TEST_ITEMS.add(new Item("Android Studio - Unable to debug when default activity is none", new DateTime(2015, 8, 1, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("Android Studio - Launch hangs if no sources changed", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Once you are connected to a BLE device, if the peripheral send service change notification, gattcallback does not have callback for that", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("undesirable behavior of sdk manager ", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Glitching real bad. No water dmg", new DateTime(2015, 8, 1, 12, 0).toDate(), 1));

        TEST_ITEMS.add(new Item("Disconnecting your device from AS should leave an entry in the Logcat View", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Debugger Not Displaying Method Arguments as Variables", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Theme declared in previous version not honored in AndroidStudio 1.4 (AI-141.2161099)", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("android.transition.Recolor doesn't animate background color", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Allo merging os multiple resource folder", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Preview Pane Shows Png resource instead of vector drawable.", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("KitKat 19 x86 Google images ", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("java.lang.AssertionError", new DateTime(2015, 8, 2, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("Race condition and potential live-lock in IssueRegistry#getIssue(String)", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Unable to delete AVD", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android Studio stuck when Scanning files to index, cannot launch app", new DateTime(2015, 8, 2, 12, 0).toDate(), 1));

        TEST_ITEMS.add(new Item("method for determining opengl extensions doesn't compile", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Start multiple devices at a time", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("MediaRecorder FPS drops after second recording.", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Insufficient Context in Tutorial", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("App moving in Lollipop", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Logcat filter by package name doesn't seem to work", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Logcat Custom filters should have opt-out of regex ", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Notifations Settings", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Allow Non-ASCII characters in project path", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Animation editor", new DateTime(2015, 8, 3, 12, 0).toDate(), 1));

        TEST_ITEMS.add(new Item("How to ask user for permission to use NotificationListenerService?", new DateTime(2015, 8, 4, 12, 0).toDate(), 4));
        TEST_ITEMS.add(new Item("Android Studio cannot run on first start", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("JNI libraries messed up after upgrade", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Pluggable Android database connectivity interface", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("android  sdk manager", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Timezone TEST_ITEMS not containing Mongolian cities.", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Decode a Scaled Image: inPurgeable is deprecated and documentation should note this/provide alternative.", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Error on build app after studio upgrade", new DateTime(2015, 8, 4, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android Studio 141.2161099 for MAC might have a memory leak", new DateTime(2015, 8, 4, 12, 0).toDate(), 2));

        TEST_ITEMS.add(new Item("Collecting Data...", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Bug: false positive on needing-to-sync gradle, when removing comments", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("com.android.test gradle plugin for test-only modules fails on Google Play Services dependency", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Theme Editor switch Theme ", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Split audio capability", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Standard keyboard shortcuts don't close AVD Manager window", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Manifest merger does not honor minSdkVersion in androidTest manifest", new DateTime(2015, 8, 5, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Bidirectional text editing", new DateTime(2015, 8, 5, 12, 0).toDate(), 4));

        TEST_ITEMS.add(new Item("Activity. shouldShowRequestPermissionRationale should take permissions as string array", new DateTime(2015, 8, 6, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Drag and Drop Quit Working", new DateTime(2015, 8, 6, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("android update sdk filter does not work with SDK Manager UI", new DateTime(2015, 8, 6, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Android Gradle 1.3.1 NullPointerException", new DateTime(2015, 8, 6, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Sample development app doesn't compile with android:cantSaveState application tag", new DateTime(2015, 8, 6, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Support IntelliJ 15 console override feature when it becomes available to us", new DateTime(2015, 8, 6, 12, 0).toDate(), 2));

        TEST_ITEMS.add(new Item("Same code crashes in one project but not another. (Only on 5.1.1 with Android Studio)", new DateTime(2015, 8, 7, 12, 0).toDate(), 3));
        TEST_ITEMS.add(new Item("Message forwarding issue", new DateTime(2015, 8, 7, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("ViewFlipper does not restart flipping after screen turns off", new DateTime(2015, 8, 7, 12, 0).toDate(), 3));
        TEST_ITEMS.add(new Item("error en el cuadro del perfil", new DateTime(2015, 8, 7, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("OnServiceConnected call very undeterministic", new DateTime(2015, 8, 7, 12, 0).toDate(), 2));

        TEST_ITEMS.add(new Item("deadlock on launch", new DateTime(2015, 8, 8, 12, 0).toDate(), 2));
        TEST_ITEMS.add(new Item("Data Binding & Espresso: IllegalAccessError: Class ref in pre-verified class resolved to unexpected implementation", new DateTime(2015, 8, 8, 12, 0).toDate(), 2));

        TEST_ITEMS.add(new Item("Help me", new DateTime(2015, 8, 9, 12, 0).toDate(), 1));
        TEST_ITEMS.add(new Item("Virtual Device Manager Configuration advanced settings window scrolling produces graphics artefacts", new DateTime(2015, 8, 9, 12, 0).toDate(), 3));

        TEST_ITEMS.add(new Item("ActionBarActivity is deprecated. But used in documentation", new DateTime(2015, 8, 10, 12, 0).toDate(), 2));
    }
}
