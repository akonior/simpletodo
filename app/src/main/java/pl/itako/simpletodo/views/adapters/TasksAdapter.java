package pl.itako.simpletodo.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.itako.simpletodo.R;
import pl.itako.simpletodo.models.Item;
import pl.itako.simpletodo.models.TestData;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMM dd");

    private final Context context;
    private final List<Item> items;


    public TasksAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.task_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);
        holder.content.setText(item.content);
        holder.dueDate.setText(DATE_FORMAT.format(item.dueDate));
        holder.pririty.setText(String.format(context.getString(R.string.priority), item.priority));
    }

    @Override
    public long getItemId(int position) {
        return TestData.TEST_ITEMS.indexOf(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.content) TextView content;
        @Bind(R.id.dueDate) TextView dueDate;
        @Bind(R.id.pririty) TextView pririty;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
