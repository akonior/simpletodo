package pl.itako.simpletodo.filter;

import java.util.ArrayList;
import java.util.List;

import pl.itako.simpletodo.models.FilterResult;
import pl.itako.simpletodo.models.Item;
import pl.itako.simpletodo.models.Section;
import pl.itako.simpletodo.models.TestData;
import pl.itako.simpletodo.models.filter.PreparedQuery;
import pl.itako.simpletodo.models.filter.QueryParser;

public class FilteringEngine {

    private final QueryParser queryParser;

    public FilteringEngine() {
        queryParser = new QueryParser();
    }

    public FilterResult filter(String query) {
        PreparedQuery preparedQuery = parseInput(query);
        List<Item> filteredItems = filter(TestData.TEST_ITEMS, preparedQuery);
        return new FilterResult(Section.from(preparedQuery), filteredItems);
    }

    List<Item> filter(List<Item> input, PreparedQuery preparedQuery) {
        List<Item> result = new ArrayList<>();
        if (preparedQuery == null) {
            return result;
        }
        for (int i = 0; i < input.size(); i++) {
            if (preparedQuery.predicate.apply(input.get(i))) {
                result.add(input.get(i));
            }
        }
        return result;
    }

    PreparedQuery parseInput(String query) {
        if (query == null || query.trim().length() == 0) {
            return new PreparedQuery("All items", new PreparedQuery.TruePredicate());
        } else {
            return queryParser.parse(query);
        }
    }
}
