package pl.itako.simpletodo.filter;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import pl.itako.simpletodo.models.Item;
import pl.itako.simpletodo.models.TestData;

public class FilteringEngineTest {

    private FilteringEngine filteringEngine;

    @Before
    public void before() {
        filteringEngine = new FilteringEngine();
    }

    @Test
    public void testPriorityFilter() {
        int all = TestData.TEST_ITEMS.size();

        List<Item> p1 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p1"));
        List<Item> p2 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p2"));
        List<Item> p3 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p3"));
        List<Item> p4 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p4"));

        Assert.assertEquals(all, filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p1|p2|p3|p4")).size());
        Assert.assertEquals(0, filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("p1&p2")).size());
        Assert.assertEquals(all, p1.size() + p2.size() + p3.size() + p4.size());
    }

    @Test
    public void testDateFilter() {
        List<Item> august9 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("2015-08-09"));
        Assert.assertEquals(2, august9.size());

        List<Item> august10 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("2015-08-10"));
        Assert.assertEquals(1, august10.size());

        List<Item> august9or10 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("2015-08-10 | 2015-08-09"));
        Assert.assertEquals(3, august9or10.size());

        List<Item> august9and10 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("2015-08-10 &  2015-08-09"));
        Assert.assertEquals(0, august9and10.size());
    }

    @Test
    public void testContentFilter() {
        List<Item> q1 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("Sample development "));
        Assert.assertEquals(1, q1.size());

        List<Item> q2 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("error"));
        Assert.assertEquals(4, q2.size());
    }

    @Test
    public void testCompoundQuery() {
        List<Item> q3 = filteringEngine.filter(TestData.TEST_ITEMS, filteringEngine.parseInput("error & ( 2015-08-02 | 2015-08-04 ) & ( p1 | p2 )"));
        Assert.assertEquals(2, q3.size());
    }
}